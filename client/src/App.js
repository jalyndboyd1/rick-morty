import React, { useState, useEffect } from "react";
import "./App.css";

const API_KEY = "https://rickandmortyapi.com/api/character";

function App() {
  const [people, setPeople] = useState([]);

  useEffect(() => {
    fetch(`${API_KEY}`)
      .then((response) => response.json())
      .then((data) => setPeople(data.results));
  }, []);
  console.log(people);
  return (
    <div>
      <center>
        <h1>Rick and Morty API</h1>
      </center>
      <div className="person">
        {people.map((person) => {
          return (
            <div key={person.id} className="individual">
              <h2>{person.name}</h2>
              <img src={person.image} alt="character" />
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default App;
